package Tests;

import ClasesAux.ArcoNoExisteException;
import ClasesAux.ArcoYaExisteException;
import ClasesAux.Iterador;
import ClasesAux.VerticeNoExisteException;
import ClasesAux.VerticeYaExisteException;
import Grafos.Camino;
import GrafosNoDirigidos.GrafoNoDirigido;
import junit.framework.TestCase;

public class GrafoNoDirigidoTest extends TestCase
{
// -----------------------------------------------------------------
// Atributos
// -----------------------------------------------------------------

/**
* Grafo sobre el que se van a hacer las pruebas.
*/
private GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico> grafo;

// -----------------------------------------------------------------
// Escenarios de prueba
// -----------------------------------------------------------------

/**
* Crea un grafo vacio
*/
public void setupEscenario1( )
{
// Crear al grafo vacio
grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );
}

/**
* Crea un grafo con 5 v�rtices y 0 arcos
*/
public void setupEscenario2( )
{
// Crear el grafo vacio
grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

// Crear los v�rtices
try
{
VerticeNumerico vn = new VerticeNumerico( 0 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 1 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 2 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 3 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 4 );
grafo.agregarVertice( vn );
}
catch( VerticeYaExisteException e )
{
// Esto no deber� a suceder
fail( );
}
}

/**
* Crea un grafo con 200 v�rtices y 0 arcos
*/
public void setupEscenario3( )
{
// Crear el grafo vacio
grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

// Crear los v�rtices
try
{
for( int i = 0; i < 200; i++ )
{
// Encontrar un v�rtice no presente en el grafo y agregarlo
int vertice;
do
{
vertice = ( int ) ( Math.random( ) * 200 );
} while( grafo.existeVertice( vertice ) );
grafo.agregarVertice( new VerticeNumerico( vertice ) );
}
}
catch( VerticeYaExisteException e )
{
// Esto no deber� a suceder
fail( );
}
}

/**
* Crea un grafo con 5 v�rtices en forma de lista encadenada
*/
public void setupEscenario4( )
{
// Crear el grafo vacio
grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

// Crear los v�rtices
try
{
VerticeNumerico vn = new VerticeNumerico( 0 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 1 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 2 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 3 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 4 );
grafo.agregarVertice( vn );
}
catch( VerticeYaExisteException e )
{
// Esto no deber�a suceder
fail( );
}

// Agregar los v�rtices
try
{
ArcoNumerico an = new ArcoNumerico( 7 );
grafo.agregarArco( 0, 1, an );
an = new ArcoNumerico( 1 );
grafo.agregarArco( 1, 2, an );
an = new ArcoNumerico( 1 );
grafo.agregarArco( 2, 3, an );
an = new ArcoNumerico( 1 );
grafo.agregarArco( 3, 4, an );
}
catch( VerticeNoExisteException e )
{
// Esto no deber� a
fail( );
}
catch( ArcoYaExisteException e )
{
// Esto no deber� a
fail( );
}
}

/**
* Crea un grafo con 5 v�rtices y 6 arcos
*/
public void setupEscenario5( )
{
// Crear el grafo vacio
grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

// Crear los v�rtices y los arcos
try
{
VerticeNumerico vn = new VerticeNumerico( 0 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 1 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 2 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 3 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 4 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 5 );
grafo.agregarVertice( vn );

ArcoNumerico an = new ArcoNumerico( 7 );
grafo.agregarArco( 0, 1, an );
an = new ArcoNumerico( 7 );
grafo.agregarArco( 1, 2, an );
an = new ArcoNumerico( 7 );
grafo.agregarArco( 2, 3, an );
an = new ArcoNumerico( 7 );
grafo.agregarArco( 3, 4, an );
an = new ArcoNumerico( 7 );
grafo.agregarArco( 4, 1, an );
an = new ArcoNumerico( 7 );
grafo.agregarArco( 2, 5, an );
}
catch( VerticeYaExisteException e )
{
// Esto no deber� a suceder
fail( );
}
catch( VerticeNoExisteException e )
{
// Esto no deber� a suceder
fail( );
}
catch( ArcoYaExisteException e )
{
// Esto no deber� a suceder
fail( );
}
}

/**
* Crea un grafo con 9 v�rtices y 10 arcos
*/
public void setupEscenario6( )
{
// Crear el grafo vacio
grafo = new GrafoNoDirigido<Integer, VerticeNumerico, ArcoNumerico>( );

// Crear los v�rtices y los arcos
try
{
VerticeNumerico vn = new VerticeNumerico( 0 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 1 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 2 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 3 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 4 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 5 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 6 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 7 );
grafo.agregarVertice( vn );
vn = new VerticeNumerico( 8 );
grafo.agregarVertice( vn );

ArcoNumerico an = new ArcoNumerico( 1 );
grafo.agregarArco( 0, 1, an );
an = new ArcoNumerico( 201 );
grafo.agregarArco( 1, 2, an );
an = new ArcoNumerico( 2 );
grafo.agregarArco( 2, 3, an );
an = new ArcoNumerico( 3 );
grafo.agregarArco( 0, 4, an );
an = new ArcoNumerico( 80 );
grafo.agregarArco( 4, 5, an );
an = new ArcoNumerico( 130 );
grafo.agregarArco( 5, 6, an );
an = new ArcoNumerico( 5 );
grafo.agregarArco( 6, 3, an );
an = new ArcoNumerico( 34 );
grafo.agregarArco( 4, 7, an );
an = new ArcoNumerico( 55 );
grafo.agregarArco( 7, 8, an );
an = new ArcoNumerico( 21 );
grafo.agregarArco( 8, 6, an );
}
catch( VerticeYaExisteException e )
{
// Esto no deber� a suceder
fail( );
}
catch( VerticeNoExisteException e )
{
// Esto no deber� a suceder
fail( );
}
catch( ArcoYaExisteException e )
{
// Esto no deber� a suceder
fail( );
}
}

// -----------------------------------------------------------------
// Pruebas para m�todos modificadores
// -----------------------------------------------------------------

/**
* Pruebas de casos normales para el m�todo <code>agregarVertice</code>
*/
public void testAgregarVertice( )
{
// Crear el grafo vacio
setupEscenario1( );

// Casos de agregar en un grafo vacio
int nVerticesPrueba = 200;
try
{
for( int i = 0; i < nVerticesPrueba; i++ )
{
VerticeNumerico vn = new VerticeNumerico( i );
grafo.agregarVertice( vn );
assertEquals( "El grafo deber�a tener " + ( i + 1 ) + " vertice", i + 1, grafo.darOrden( ) );
assertEquals( "El v�rtice en el grafo no corresponde al agregado", vn.darId( ), grafo.darVertice( i ).darId( ) );
assertEquals( "El v�rtice en el grafo no corresponde al agregado", vn, grafo.darVertice( i ) );
}
}
catch( VerticeYaExisteException e )
{
fail( "El v�rtice " + e.darIdentificador( ) + " agregado no existia. Debio ser agregado existosamente." );
e.printStackTrace( );
}
catch( VerticeNoExisteException e )
{
fail( "El v�rtice " + e.darIdentificador( ) + " no fue agregado exitosamente" );
e.printStackTrace( );
}
}

/**
* Pruebas de casos erroneos para el m�todo <code>agregarVertice</code>
*/
public void testAgregarVerticeError( )
{
setupEscenario2( );

// Ingresar un vertice repetido
try
{
VerticeNumerico vn = new VerticeNumerico( 0 );
grafo.agregarVertice( vn );
fail( "El v�rtice ingresado ya existe" );
}
catch( VerticeYaExisteException e )
{
// Este es el comportamiento esperado
}

// Ingresar un vertice repetido
try
{
VerticeNumerico vn = new VerticeNumerico( 2 );
grafo.agregarVertice( vn );
fail( "El v�rtice ingresado ya existe" );
}
catch( VerticeYaExisteException e )
{
// Este es el comportamiento esperado
}
}

/**
* Pruebas de casos normales para el m�todo <code>eliminarVertice</code>
*/
public void testEliminarVertice( )
{
// Crear un grafo con 200 v�rtices
setupEscenario3( );

// Eliminar todos los v�rtices del grafo en orden aleatorio
try
{
for( int i = 0; i < 200; i++ )
{
// Encontrar un v�rtice presente en el grafo
int vertice;
do
{
vertice = ( int ) ( Math.random( ) * 200 );
} while( !grafo.existeVertice( vertice ) );

grafo.eliminarVertice( vertice );
assertEquals( "El grafo deber�a tener " + ( 199 - i ) + " vertice", 199 - i, grafo.darOrden( ) );
assertFalse( "El v�rtice " + vertice + " no fue eliminado", grafo.existeVertice( vertice ) );
}
assertEquals( "El grafo deber�a tener 0 vertice", 0, grafo.darOrden( ) );
}
catch( VerticeNoExisteException e )
{
fail( e.getMessage( ) );
}

// Reingresar los v�rtices para comprobar que no los toma como repetidos
try
{
for( int i = 0; i < 200; i++ )
{
// Encontrar un v�rtice no presente en el grafo
int vertice;
do
{
vertice = ( int ) ( Math.random( ) * 200 );
} while( grafo.existeVertice( vertice ) );

grafo.agregarVertice( new VerticeNumerico( vertice ) );
}
}
catch( VerticeYaExisteException e )
{
fail( e.getMessage( ) );
}
}

/**
* Pruebas de casos erroneos para el m�todo <code>eliminarVertice</code>
*/
public void testEliminarVerticeError( )
{
setupEscenario1( );

// Eliminar un v�rtice inexistente
try
{
grafo.eliminarVertice( 6 );
fail( "El v�rtice eliminado no existe" );
}
catch( VerticeNoExisteException e )
{
// Este es el comportamiento esperado
}

setupEscenario2( );

// Eliminar un v�rtice inexistente
try
{
grafo.eliminarVertice( 6 );
fail( "El v�rtice eliminado no existe" );
}
catch( VerticeNoExisteException e )
{
// Este es el comportamiento esperado
}
}

/**
* Pruebas de casos normales para el m�todo <code>agregarArco</code>
*/
public void testAgregarArco( )
{
// Cargar un grafo con 200 vertices
setupEscenario3( );

// Agregar 50 arcos aleatorios
try
{
for( int i = 0; i < 50; i++ )
{
// Encontrar dos v�rtices que no tengan arco entre ellos
int vertice1, vertice2;

do
{
vertice1 = ( int ) ( Math.random( ) * 200 );
vertice2 = ( int ) ( Math.random( ) * 200 );
} while( grafo.existeArco( vertice1, vertice2 ) );
grafo.agregarArco( vertice1, vertice2, new ArcoNumerico( 7 ) );
assertEquals( "El arco no fue registrado exitosamente", i + 1, grafo.darNArcos( ) );
assertTrue( "El arco no fue registrado exitosamente", grafo.existeArco( vertice1, vertice2 ) );
}
}
catch( VerticeNoExisteException e )
{
// Esto no deber�a suceder
fail( e.getMessage( ) );
e.printStackTrace( );
}
catch( ArcoYaExisteException e )
{
e.printStackTrace( );
fail( "El arco entre " + e.darOrigen( ) + " y " + e.darDestino( ) + " deb�o ser ingresado existosamente" );
}

}

/**
* Pruebas de casos erroneos para el m�todo <code>agregarArco</code>
*/
public void testAgregarArcoError( )
{
// Crear un grafo con 5 vertices en forma de lista encadenada
setupEscenario4( );

// Ingresar un arco entre vertices inexistentes
try
{
grafo.agregarArco( 1, 6, new ArcoNumerico( 7 ) );
fail( "Uno de los v�rtices conectados por el arco no existe" );
}
catch( VerticeNoExisteException e )
{
// Comportamiento esperado
}
catch( ArcoYaExisteException e )
{
fail( "Uno de los v�rtices conectados por el arco no existe" );
}
try
{
grafo.agregarArco( 6, 1, new ArcoNumerico( 7 ) );
fail( "Uno de los v�rtices conectados por el arco no existe" );
}
catch( VerticeNoExisteException e )
{
// Comportamiento esperado
}
catch( ArcoYaExisteException e )
{
fail( "Uno de los v�rtices conectados por el arco no existe" );
}
try
{
grafo.agregarArco( 6, 6, new ArcoNumerico( 7 ) );
fail( "Uno de los v�rtices conectados por el arco no existe" );
}
catch( VerticeNoExisteException e )
{
// Comportamiento esperado
}
catch( ArcoYaExisteException e )
{
fail( "Uno de los v�rtices conectados por el arco no existe" );
}

// Ingresar un arco repetido
try
{
grafo.agregarArco( 1, 2, new ArcoNumerico( 7 ) );
fail( "Ya existe un arco entre esos vertices" );
}
catch( VerticeNoExisteException e )
{
fail( "Ya existe un arco entre esos vertices" );
}
catch( ArcoYaExisteException e )
{
// Comportamiento esperado
}
}

/**
* Pruebas de casos normales para el m�todo <code>eliminarArco</code>
*/
public void testEliminarArco( )
{
// Crear un grafo con 5 v�rtices en forma de lista encadenada
setupEscenario4( );

// Eliminar todos los arcos del grafo
try
{
for( int i = 0; i < 4; i++ )
{
grafo.eliminarArco( i, i + 1 );
assertEquals( "El arco no fue eliminado exitosamente", 4 - i - 1, grafo.darNArcos( ) );
assertFalse( "El arco no fue eliminado exitosamente", grafo.existeArco( i, i + 1 ) );
}
assertEquals( "El grafo deb�o quedar sin arcos", 0, grafo.darNArcos( ) );
}
catch( VerticeNoExisteException e )
{
fail( "El vertice " + e.darIdentificador( ) + " no fue encontrado" );
}
catch( ArcoNoExisteException e )
{
fail( "El arco entre los v�rtices " + e.darOrigen( ) + " y " + e.darDestino( ) + " no fue encontrados" );
}
}

/**
* Pruebas de casos erroneos para el m�todo <code>eliminarArco</code>
*/
public void testEliminarArcoError( )
{
// Crear un grafo con 5 v�rtices en forma de lista encadenada
setupEscenario4( );

// Tratar de eliminar un arco entre v�rtices que no existen
try
{
grafo.eliminarArco( 0, 6 );
fail( "El arco no deb�o ser eliminado exitosamente" );
}
catch( VerticeNoExisteException e )
{
// Comportamiento esperado
}
catch( ArcoNoExisteException e )
{
fail( "El v�rtice 6 no existe" );
}
try
{
grafo.eliminarArco( 6, 0 );
fail( "El arco no deb�o ser eliminado exitosamente" );
}
catch( VerticeNoExisteException e )
{
// Comportamiento esperado
}
catch( ArcoNoExisteException e )
{
fail( "El v�rtice 6 no existe" );
}

// Tratar de eliminar un arco que no existe
try
{
grafo.eliminarArco( 0, 2 );
}
catch( VerticeNoExisteException e )
{
fail( "El v�rtice " + e.darIdentificador( ) + " si existe" );
}
catch( ArcoNoExisteException e )
{
// Comportamiento esperado
}
}

/**
* Pruebas de casos normales para el m�todo <code>hayCamino</code>
*/
public void testHayCamino( )
{
setupEscenario5( );

try
{
assertTrue( "Algoritmo invalido para detecci�n de caminos", grafo.hayCamino( 0, 3 ) );
assertTrue( "Algoritmo invalido para detecci�n de caminos", grafo.hayCamino( 0, 5 ) );
assertFalse( "Algoritmo invalido para detecci�n de caminos", grafo.hayCamino( 0, 0 ) );
}
catch( VerticeNoExisteException e )
{
fail( "el vertice " + e.darIdentificador( ) + " si existe" );
}
}

/**
* Pruebas de casos normales para el m�todo <code>darCaminoMasCorto</code>
*/
public void testDarCaminoMasCorto( )
{
setupEscenario6( );
try
{
Camino<Integer, VerticeNumerico, ArcoNumerico> camino = grafo.darCaminoMasCorto( 0, 3 );
assertNotNull( "Algoritmo invalido para calcular el camino m�s corto", camino );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", 3, camino.darLongitud( ) );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", new Integer( 0 ), camino.darOrigen( ).darId( ) );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", 204, camino.darCosto( ) );

Iterador<VerticeNumerico> iter = camino.darSecuenciaVertices( );
assertEquals( "Camino mal formado", new Integer( 0 ), iter.darSiguiente( ).darId( ) );
assertEquals( "Camino mal formado", new Integer( 1 ), iter.darSiguiente( ).darId( ) );
assertEquals( "Camino mal formado", new Integer( 2 ), iter.darSiguiente( ).darId( ) );
assertEquals( "Camino mal formado", new Integer( 3 ), iter.darSiguiente( ).darId( ) );

camino = grafo.darCaminoMasCorto( 6, 6 );
assertNotNull( "Algoritmo invalido para calcular el camino m�s corto", camino );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", 5, camino.darLongitud( ) );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", new Integer( 6 ), camino.darOrigen( ).darId( ) );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", 320, camino.darCosto( ) );
}
catch( VerticeNoExisteException e )
{
fail( "El vertice " + e.darIdentificador( ) + " si existe" );
}
}

/**
* Pruebas de casos normales para el m�todo <code>darCaminoMasBarato</code>
*/
public void testDarCaminoMasBarato( )
{
setupEscenario6( );
try
{
Camino<Integer, VerticeNumerico, ArcoNumerico> camino = grafo.darCaminoMasBarato( 0, 3 );
assertNotNull( "Algoritmo invalido para calcular el camino m�s corto", camino );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", 5, camino.darLongitud( ) );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", new Integer( 0 ), camino.darOrigen( ).darId( ) );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", 118, camino.darCosto( ) );

Iterador<VerticeNumerico> iter = camino.darSecuenciaVertices( );
assertEquals( "Camino mal formado", new Integer( 0 ), iter.darSiguiente( ).darId( ) );
assertEquals( "Camino mal formado", new Integer( 4 ), iter.darSiguiente( ).darId( ) );
assertEquals( "Camino mal formado", new Integer( 7 ), iter.darSiguiente( ).darId( ) );
assertEquals( "Camino mal formado", new Integer( 8 ), iter.darSiguiente( ).darId( ) );
assertEquals( "Camino mal formado", new Integer( 6 ), iter.darSiguiente( ).darId( ) );
assertEquals( "Camino mal formado", new Integer( 3 ), iter.darSiguiente( ).darId( ) );

camino = grafo.darCaminoMasBarato( 3, 3 );
assertNotNull( "Algoritmo invalido para calcular el camino m�s corto", camino );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", 8, camino.darLongitud( ) );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", new Integer( 3 ), camino.darOrigen( ).darId( ) );
assertEquals( "Algoritmo invalido para calcular el camino m�s corto", 322, camino.darCosto( ) );
}
catch( VerticeNoExisteException e )
{
fail( "El vertice " + e.darIdentificador( ) + " si existe" );
}
}

/**
* Pruebas de casos normales para el m�todo <code>hayCiclo</code>
*/
public void testHayCiclo( )
{
try
{
setupEscenario5( );
assertTrue( "En este grafo si hay ciclo", grafo.hayCiclo( 1 ) );

setupEscenario6( );
assertTrue( "En este grafo si hay ciclo", grafo.hayCiclo( 5 ) );
assertTrue( "En este grafo si hay ciclo", grafo.hayCiclo( 0 ) );
assertTrue( "En este grafo si hay ciclo", grafo.hayCiclo( 8 ) );
assertTrue( "En este grafo si hay ciclo", grafo.hayCiclo( 3 ) );
assertTrue( "En este grafo si hay ciclo", grafo.hayCiclo( 6 ) );

setupEscenario5( );
assertFalse( "En este grafo no hay ciclo", grafo.hayCiclo( 5 ) );
}
catch( VerticeNoExisteException e )
{
fail( "El vertice " + e.darIdentificador( ) + " si existe" );
}
}

}
 