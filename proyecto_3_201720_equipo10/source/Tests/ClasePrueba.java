package Tests;

public class ClasePrueba 
{

	private String nombre;
	private int codigo;
	
	public ClasePrueba(String n, int c)
	{
		nombre = n;
		codigo = c;
		
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public int darCodigo()
	{
		return codigo;
	}
	
}
