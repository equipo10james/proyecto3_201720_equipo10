package Tests;

import ClasesAux.IArco;

public class ArcoNumerico implements IArco
{
// -----------------------------------------------------------------
// Constantes
// -----------------------------------------------------------------

/**
* Constante para la serialización 
*/
private static final long serialVersionUID = 1L;

// -----------------------------------------------------------------
// Atributos
// -----------------------------------------------------------------

/**
* Peso del arco.
*/
private int peso;

// -----------------------------------------------------------------
// Constructores
// -----------------------------------------------------------------

/**
* Constructor por parámetros.
* @param peso Peso del arco.
*/
public ArcoNumerico( int peso )
{
this.peso = peso;
}

// -----------------------------------------------------------------
// Métodos
// -----------------------------------------------------------------

/* (non-Javadoc)
* @see uniandes.cupi2.collections.grafo.IArco#darPeso()
*/
public int darPeso( )
{
return peso;
}

}
 