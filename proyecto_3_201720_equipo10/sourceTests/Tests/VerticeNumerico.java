package Tests;

import ClasesAux.IVertice;

public class VerticeNumerico implements IVertice<Integer>
{
// -----------------------------------------------------------------
// Constantes
// -----------------------------------------------------------------

/**
* Constante para la serialización 
*/
private static final long serialVersionUID = 1L;

// -----------------------------------------------------------------
// Atributos
// ----------------------------------------------------------------- 

/**
* Dato contenido por el vértice.
*/
private int valor;

// -----------------------------------------------------------------
// Constructores
// ----------------------------------------------------------------- 

/**
* Constructor de la clase.
* @param valor Dato contenido por el vértice.
*/
public VerticeNumerico( int valor )
{
this.valor = valor;
}

// -----------------------------------------------------------------
// Métodos
// ----------------------------------------------------------------- 

/*
* (non-Javadoc)
* @see uniandes.cupi2.collections.grafo.IVertice#darId()
*/
public Integer darId( )
{
return valor;
}
}