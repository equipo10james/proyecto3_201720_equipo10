package model.logic;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.Gson;

import Grafos.CC;
import Grafos.GrafoDirigido;
import api.ISTSManager;
import model.data_structures.HashTableChaining;
import model.data_structures.HashTableProbing;
import model.data_structures.MaxPQ;
import model.data_structures.RingList;
import model.data_structures.RojoNegro;
import model.vo.Itinerario;
import model.vo.RangoHora;
import model.vo.VOAgency;
import model.vo.VOBusUpdate;
import model.vo.VOCalendar;
import model.vo.VOCalendarDates;
import model.vo.VOFeedInfo;
import model.vo.VORetraso;
import model.vo.VORoute;
import model.vo.VOShape;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTransfers;
import model.vo.VOTrip;

public class STSManager implements ISTSManager 
{

	//------------------------------------
	// ATRIBUTOS
	//------------------------------------

	private HashTableProbing<String, VORoute> rutasProbing;
	private HashTableChaining<String, VORoute> rutasChaining;

	private RingList<VOStopTimes> listaStopTimes;

	private HashTableChaining<String, VOTrip> viajesChaining;


	private HashTableChaining<String, VOStop> listaParadasChaining;


	private RingList<VOBusUpdate> actualizacionBus;


	private HashTableChaining<String, VOAgency> listaAgencias;

	private HashTableChaining<String, VOCalendar> calendarios;

	private RingList<VOCalendarDates> excepcionesCalendario;

	private RingList<VOFeedInfo> listaFeed;

	private RingList<VORetraso> listaRetrasos;

	private RingList<VOShape> listaShapes;

	private RingList<VOTransfers> listaTransfers;


	private GrafoDirigido grafo1;
	private GrafoDirigido grafo2;


	//------------------------------------
	// METODOS DE LEER LOS ARCHIVOS
	//------------------------------------

	public void readBusUpdate(File rtFile) 
	{
		actualizacionBus = new RingList<VOBusUpdate>();
		Gson gson = new Gson();

		try
		{

			VOBusUpdate[] info = gson.fromJson(new FileReader(rtFile), VOBusUpdate[].class);
			for (int i = 0; i < info.length; i++) 
			{
				VOBusUpdate currentBus = info[i];
				actualizacionBus.addLast(currentBus);

				String tripId = currentBus.trip_id();

				VOTrip viajeActual = viajesChaining.get(tripId);

				if(viajeActual != null)
				{
					viajeActual.agregarUpdate(currentBus);
				}
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	//Carga las rutas tanto en tabla de hash probing como en chaining
	public void loadRoutes(String routesFile) 
	{
		try 
		{
			rutasChaining = new HashTableChaining<String, VORoute>(6007);
			rutasProbing = new HashTableProbing<String, VORoute>(3001);


			BufferedReader br = new BufferedReader(new FileReader(routesFile));

			String linea = br.readLine();
			linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");


				String route_color = "";
				String route_text_color = "";

				if(info.length==10)
				{
					route_color = info[7];

					route_text_color = info[8];
				}


				VORoute ruta = new VORoute (info[0], info[1], info[2], info[3], info[4], info[5], info[6],  route_color, route_text_color);
				//System.out.println(ruta);


				rutasChaining.put(info[0], ruta);
				rutasProbing.put(info[0], ruta);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	//Carga los viajes tanto en lista como en tabla de hash chaining (con llave el tripid)
	public void loadTrips(String tripsFile) 
	{
		try 
		{
			viajesChaining = new HashTableChaining<String, VOTrip>(6007);

			BufferedReader br = new BufferedReader(new FileReader(tripsFile));
			String linea = br.readLine();
			linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOTrip viaje = new VOTrip (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9]);
				//System.out.println(viaje);

				//Carga el calendario del viaje
				VOCalendar calendario = calendarios.get(viaje.service_id());
				viaje.agregarCalendario(calendario);

				//Cargar viajes a rutas
				VORoute ruta = rutasChaining.get(viaje.route_id());

				if(rutasChaining.contains(viaje.route_id()))
					ruta.agregarViaje(viaje);

				viajesChaining.put(info[2], viaje);

				linea = br.readLine();
			}

			br.close();
		}


		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}

	//Carga los stoptimes en una lista 
	public void loadStopTimes(String stopTimesFile) 
	{

		try 
		{
			listaStopTimes = new RingList<VOStopTimes>();

			BufferedReader br = new BufferedReader(new FileReader(stopTimesFile));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");


				String shape = new String();

				if(info.length==8)
				{
					shape="";
				}
				else if (info.length==9)
				{
					shape = info[8];
				}


				VOStopTimes stopTime = new VOStopTimes (info[0], info[1], info[2], info[3], info[4],info[5], info[6], info[7], shape);

				VOTrip viaje = viajesChaining.get(stopTime.trip_id());

				//Agregar Viajes a Paradas en hash
				viaje.actualizarTiempoLlegada(stopTime.arrival_time());


				VOStop parada = listaParadasChaining.get(stopTime.stop_id());

				if(listaParadasChaining.contains(stopTime.stop_id()))
					parada.agregarViaje(viaje.trip_id());


				//Agregar stopTimes a Trip en hash
				viaje.agregarStopTimes(stopTime);



				//agrega parada a viaje
				viaje.agregarParada(parada);

				//System.out.println(stopTime);
				listaStopTimes.addLast(stopTime);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	//Carga las paradas tanto en una lista como en una tabla de hash con chaining
	public void loadStops(String stopsFile) 
	{
		try 
		{
			listaParadasChaining = new HashTableChaining<String, VOStop>(6007);


			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOStop parada = new VOStop (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], "");

				listaParadasChaining.put(info[0], parada);

				linea = br.readLine();
			}

			br.close();

		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}

	public void loadAgency(String agencyFiles)
	{
		try 
		{
			listaAgencias = new HashTableChaining<String, VOAgency>(6007);

			BufferedReader br = new BufferedReader(new FileReader(agencyFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOAgency agencia = new VOAgency (info[0], info[1], info[2], info[3], info[4]);
				//System.out.println(agencia);
				listaAgencias.put(info[0], agencia);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo agency");
	}

	public void loadCalendar(String calendarFiles)
	{
		try 
		{
			calendarios = new HashTableChaining<String, VOCalendar>(6007);

			BufferedReader br = new BufferedReader(new FileReader(calendarFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				int startDate = Integer.parseInt(info[8]);
				int endDate = Integer.parseInt(info[9]);

				VOCalendar calendario = new VOCalendar (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], startDate, endDate);
				//System.out.println(calendario);
				calendarios.put(info[0], calendario);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo calendar");
	}



	public void loadCalendar_dates(String calendar_datesFiles)
	{
		try 
		{
			excepcionesCalendario = new RingList<VOCalendarDates>();

			BufferedReader br = new BufferedReader(new FileReader(calendar_datesFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");


				VOCalendarDates calendarException = new VOCalendarDates(info[0], info[1], info[2]);
				//	System.out.println(calendarException);


				excepcionesCalendario.addLast(calendarException);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo exceptions");
	}

	public void loadShapes(String shapesFiles)
	{
		try 
		{
			listaShapes = new RingList<VOShape>();

			BufferedReader br = new BufferedReader(new FileReader(shapesFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOShape shape = new VOShape (info[0], info[1], info[2], info[3], info[4]);
				//System.out.println(shape);
				listaShapes.addLast(shape);


				linea = br.readLine();
			}


			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo shapes");

	}

	public void loadFeedInfo(String feed_infoFiles)
	{
		try 
		{
			listaFeed = new RingList<VOFeedInfo>();

			BufferedReader br = new BufferedReader(new FileReader(feed_infoFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOFeedInfo feed = new VOFeedInfo (info[0], info[1], info[2], info[3], info[4], info[5]);
				//System.out.println(feed);
				listaFeed.addLast(feed);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		//System.out.println("cargo feedinfo");
	}

	public void loadTransfers(String transfersFiles)
	{
		try 
		{
			listaTransfers = new RingList<VOTransfers>();

			BufferedReader br = new BufferedReader(new FileReader(transfersFiles));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				String ultimo = "";

				if(info.length==4)
				{
					ultimo = info[3];
				}

				VOTransfers transfer = new VOTransfers (info[0], info[1], info[2], ultimo);
				//System.out.println(transfer);
				listaTransfers.addLast(transfer);


				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

		// System.out.println("cargo transfers");
	}


	//------------------------------------
	// METODOS AUXILIARES DE PARADAS
	//------------------------------------



	//Recorre las paradas de todos los viajes en una fecha dada para guardar todas las paradas de la ruta en la fecha
	public HashTableChaining<String, VOStop> darParadasRutaEnFecha(String idRuta, String fecha)
	{
		RingList<VOTrip> viajesEnFecha = darViajesFecha(fecha);
		HashTableChaining<String, VOStop> paradas = new HashTableChaining<String, VOStop>(6007);

		for (int i = 0; i < viajesEnFecha.size(); i++) 
		{
			if(((VOTrip) viajesEnFecha.getElement(i)).route_id().equals(idRuta))
			{

				RingList<VOStop> paradasViaje = ((VOTrip) viajesEnFecha.getElement(i)).darParadas();

				for (int j = 0; j < paradasViaje.size(); j++) 
				{
					if(!paradas.contains( ((VOStop) paradasViaje.getElement(j)).id() ))
					{
						paradas.put(((VOStop) paradasViaje.getElement(j)).id(), (VOStop) paradasViaje.getElement(j));
					}

				}
			}
		}

		return paradas;
	}


	//------------------------------------
	// METODOS AUXILIARES DE VIAJES
	//------------------------------------


	//Carga los shapes y las excepciones para cada viaje en la tabla de hash chaining
	public void agregarInfoExtraViajesChaining()
	{
		Iterator<String> iterator =  viajesChaining.keys();

		while(iterator.hasNext())
		{
			VOTrip viajeActual = viajesChaining.get(iterator.next());

			//Carga Shapes
			Iterator<VOShape> iterator1 = listaShapes.iterator();

			while(iterator1.hasNext())
			{
				VOShape shapeActual = iterator1.next();

				if(viajeActual.shape_id().equals(shapeActual.shape_id()))
				{
					viajeActual.agregarShape(shapeActual);
				}
			}


			//Carga Exepciones de fecha
			Iterator<VOCalendarDates> iterator2 = excepcionesCalendario.iterator();

			while(iterator2.hasNext())
			{
				VOCalendarDates exceptionActual = iterator2.next();

				if(viajeActual.service_id().equals(exceptionActual.service_id()))
				{
					viajeActual.agregarCalendarException(exceptionActual);
				}
			}
		}

	}


	//pFecha se da en formato YYYYMMDD y se retorna si hay o no una excepcion para esa fecha en el viaje dado
	public boolean fechaConExcepcion(String pFecha, String serviceId)
	{
		boolean rep = false;

		for (int i = 0; i < excepcionesCalendario.size(); i++)
		{
			VOCalendarDates exceptionActual = (VOCalendarDates) excepcionesCalendario.getElement(i);

			if(pFecha.equals( exceptionActual.date()) && exceptionActual.service_id().equals(serviceId))
			{
				rep = true;
			}
		}

		return rep;
	}


	//pFecha se da en formato YYYYMMDD y se retornan todos los viajes en dicha fecha
	public RingList<VOTrip> darViajesFecha(String pFecha)
	{
		RingList<VOTrip> viajesEnFecha = new RingList<VOTrip>();
		RingList<String> services = new RingList<String>();

		Iterator <String> iterador1 = calendarios.keys();

		while (iterador1.hasNext())
		{
			VOCalendar calendarActual = calendarios.get(iterador1.next());

			int fecha = Integer.parseInt(pFecha);

			if( calendarActual.start_date() < fecha && calendarActual.end_date() > fecha)
			{
				String dia = darDia(pFecha);

				if(dia.equalsIgnoreCase("lunes") && Integer.parseInt(calendarActual.monday()) == 1 )
				{
					if(!fechaConExcepcion(pFecha, calendarActual.service_id()))
						services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("martes") && Integer.parseInt(calendarActual.tuesday()) == 1 )
				{
					if(!fechaConExcepcion(pFecha, calendarActual.service_id()))
						services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("miercoles") && Integer.parseInt(calendarActual.wednesday()) == 1 )
				{
					if(!fechaConExcepcion(pFecha, calendarActual.service_id()))
						services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("jueves") && Integer.parseInt(calendarActual.thursday()) == 1 )
				{
					if(!fechaConExcepcion(pFecha, calendarActual.service_id()))
						services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("viernes") && Integer.parseInt(calendarActual.friday()) == 1 )
				{
					if(!fechaConExcepcion(pFecha, calendarActual.service_id()))
						services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("sabado") && Integer.parseInt(calendarActual.saturday()) == 1 )
				{
					if(!fechaConExcepcion(pFecha, calendarActual.service_id()))
						services.addLast(calendarActual.service_id());
				}
				else if(dia.equalsIgnoreCase("domingo") && Integer.parseInt(calendarActual.sunday()) == 1 )
				{
					if(!fechaConExcepcion(pFecha, calendarActual.service_id()))
						services.addLast(calendarActual.service_id());
				}
			}
		}

		Iterator<String> iterador = viajesChaining.keys();


		for (int i = 0; i < services.size(); i++) 
		{
			String serviceId  = (String) services.getElement(i);

			while(iterador.hasNext())
			{
				VOTrip viaje = viajesChaining.get(iterador.next());

				if(viaje.service_id().equals(serviceId))
				{
					System.out.println(viaje);
					viajesEnFecha.addLast(viaje);
				}
			}
		}

		return viajesEnFecha;
	}


	//pFecha se da en formato YYYYMMDD y retorna el dia de esa fecha
	private String darDia(String pFecha)
	{
		SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd");
		String dia = null;

		try 
		{
			Date fecha = formato.parse(pFecha);
			DateFormat formato1 = new SimpleDateFormat("EEEE");
			dia = formato1.format(fecha);
		} 

		catch (ParseException e) 
		{
			e.printStackTrace();
		}

		return dia;
	}


	//------------------------------------
	// METODOS AUXILIARES DE RUTAS
	//------------------------------------


	//Retorna todas las paradas con sus respectivas rutas para una fecha dada
	public HashTableChaining<VOStop, RingList<VORoute>>  rutasQuePasanParadasEnFecha(String fecha)
	{
		HashTableChaining<VOStop, RingList<VORoute>> rutasParadas = new HashTableChaining<VOStop, RingList<VORoute>>();

		RingList<VOTrip> viajesFecha = darViajesFecha(fecha);


		for (int i = 0; i < viajesFecha.size(); i++) 
		{
			VOTrip viajeActual = (VOTrip) viajesFecha.getElement(i);
			VORoute ruta = null;

			ruta = rutasChaining.get(viajeActual.route_id());


			RingList<VOStop> paradasViaje = viajeActual.darParadas();


			for (int j = 0; j < paradasViaje.size(); j++) 
			{
				VOStop paradaActual = (VOStop) paradasViaje.getElement(j);

				if(!rutasParadas.contains(paradaActual))
				{
					RingList<VORoute> rutas = new RingList<VORoute>();
					rutas.addFirst(ruta);

					rutasParadas.put(paradaActual, rutas);
				}

				else
				{
					rutasParadas.get(paradaActual).addLast(ruta);

				}	
			}
		}


		return rutasParadas;
	}



	//------------------------------------
	// METODOS DE RETRASOS
	//------------------------------------

	//Carga los retrasos de todos los viajes en la lista de retrasos
	public void cargarRetrasosLista()
	{
		RingList<VORetraso> retrasos = new RingList<VORetraso>();

		Iterator<String> iterador = viajesChaining.keys();

		while(iterador.hasNext())
		{
			VOTrip viajeActual = viajesChaining.get(iterador.next());
			RingList<VORetraso> retrasosViaje = viajeActual.darRetrasos();


			for (int j = 0; j < retrasosViaje.size(); j++) 
			{
				retrasos.addLast((VORetraso) retrasosViaje.getElement(j));
			}
		}

		listaRetrasos = retrasos;
		System.out.println("cargo retrasos lista");
	}


	//Carga los retrasos de cada viaje 
	public void cargarRetrasosViajes()
	{
		//Carga los retrasos de los viajes del chaining

		Iterator<String> iterador = viajesChaining.keys();

		while(iterador.hasNext())
		{
			VOTrip actual = viajesChaining.get(iterador.next());

			encontrarRetardos(actual);

		}

		System.out.println("cargo retrasos viajes");
	}


	//Encuentra los retrasos de el viaje ingresado por parametro
	public void encontrarRetardos(VOTrip viaje)
	{
		if(viaje != null)
		{
			//Buscar actualizaciones del bus para el viaje ingresado por parametro
			RingList<VOBusUpdate> actualizaciones = viaje.darUpdatesViajes();

			//Lista de los retrasos de viaje
			RingList<VORetraso> retrasos  = new RingList<VORetraso>();

			RingList<VOStop> paradasViaje = viaje.darParadas();
			RingList<VOStopTimes> stopTimesViaje = viaje.darStopTimes();

			//			Iterator<VOStop> iterador1 = paradasViaje.iterator();
			//			Iterator <VOStopTimes> iterador2 = stopTimesViaje.iterator();

			VORoute ruta = rutasChaining.get(viaje.route_id());


			for (int i = 0; i < paradasViaje.size(); i++) 
			{
				VOStop stopActual = (VOStop) paradasViaje.getElement(i);
				VOStopTimes stopTimeDelStop = (VOStopTimes) stopTimesViaje.getElement(i);

				double lat = Double.parseDouble(stopActual.lat());
				double lon = Double.parseDouble(stopActual.lon());



				VOBusUpdate menorCercano = null;
				VOBusUpdate mMenorCercano = null;

				double menor = Double.MAX_VALUE;
				double mMenor = Double.MAX_VALUE;


				for (int j = 0; j < actualizaciones.size(); j++) 
				{
					VOBusUpdate actual = (VOBusUpdate) actualizaciones.getElement(j);
					double latComp = Double.parseDouble(actual.latitude());
					double lonComp = Double.parseDouble(actual.longitude());
					double distancia = getDistance(lat, lon, latComp, lonComp);

					if(distancia<mMenor)
					{
						menorCercano = mMenorCercano;
						menor = mMenor;
						mMenorCercano = actual;
						mMenor = distancia;
					}

					else if (distancia<menor)
					{
						menorCercano = actual;
						menor = distancia;
					}
				}

				double retardo = 0;

				if(menorCercano != null && mMenorCercano!= null)
				{

					String horaBus1 = menorCercano.recordedTime();
					String horaBus2 = mMenorCercano.recordedTime();


					int hora1 = horaAsegundos(horaBus1);
					int hora2 = horaAsegundos(horaBus2);


					int promedio = (hora1 + hora2)/2;

					int horaStop = horaAsegundos(stopTimeDelStop.departure_time());

					retardo = horaStop-promedio;

				}

				if(retardo < 0)
				{
					VORetraso retraso = new VORetraso(stopActual, viaje, ruta, viaje.darCalendar().start_date()+"", retardo);
					retrasos.addLast(retraso);
				}

			}

			viaje.cargarRetrasos(retrasos);
		}
	}



	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		final double R = 6371*1000; // Radious of the earth
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;

		return distance;

	}

	private double toRad( double value)
	{
		return (value * Math.PI / 180);
	}

	public int horaAsegundosNormal(String hora)
	{
		int rta = 0;

		String[] arreglo= hora.split(":");

		rta = ((3600* Integer.parseInt(arreglo[0]))+(60*Integer.parseInt(arreglo[1]))+(Integer.parseInt(arreglo[2])));

	
		return rta;
	}

	//De formato HH:MM:SS o HH:MM:SS TT donde TT es o "pm" o "am" a int con segundos
	public int horaAsegundos(String hora)
	{
		int rta = 0;

		String[] arreglo= hora.split(":");

		String[] segundosAMPM = arreglo[2].split(" ");

		rta = ((3600* Integer.parseInt(arreglo[0]))+(60*Integer.parseInt(arreglo[1]))+(Integer.parseInt(segundosAMPM[0])));

		if(segundosAMPM.length==2 && segundosAMPM[1].equalsIgnoreCase("pm"))
		{
			rta += (12*3600);
		}

		return rta;
	}


	//------------------------------------
	// METODOS PARA CARGAR INFORMACION
	//------------------------------------


	@Override
	public void cargarGTFS() 
	{
		loadAgency("./data/agency.txt");
		System.out.println("cargo agencias");
		loadCalendar("./data/calendar.txt");
		System.out.println("cargo calendarios");
		loadCalendar_dates("./data/calendar_dates.txt");
		System.out.println("cargo exceptions");
		loadShapes("./data/shapes.txt");
		System.out.println("cargo shapes");
		loadFeedInfo("./data/feed_info.txt");
		System.out.println("cargo feedinfo");
		loadTransfers("./data/transfers.txt");
		System.out.println("cargo transfers");

		loadRoutes("./data/routes.txt");
		System.out.println("cargo ruta");

		loadTrips("./data/trips.txt");
		System.out.println("cargo viajes");

		loadStops("./data/stops.txt");
		System.out.println("cargo stops");

		loadStopTimes("./data/stop_times.txt");
		System.out.println("cargo stopstimes");

		System.out.println("cargo estatico");
	}

	//Fecha entra en formato AAAAMMDD
	@Override
	public void cargarTR(String fecha) 
	{
		String mes = fecha.substring(5, 6);
		String dia = fecha.substring(6);


		String fechaFinal = mes+"-"+dia; 

		File archivo1 = new File("./data/RealTime-"+fechaFinal+"-BUSES_SERVICE");
		File[] files1 = archivo1.listFiles();


		for (int i = 0; i < files1.length; i++) 
		{
			readBusUpdate(files1[i]);
		}
		
		System.out.println("cargo temp");

	}

	public void init() 
	{

		//agregarInfoExtraViajesChaining();
		//System.out.println("cargo a viajes info extra chaining");

		
		cargarRetrasosViajes();
		cargarRetrasosLista();
	}

	
	
	//------------------------------------
	// METODOS REQUERIMIENTOS
	//------------------------------------

	@Override
	public RingList<VOStop> darParadasAlcanzables(String idParada, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RingList<CC> darComponentesFuertementeConexos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RingList<VOStop> darParadasGrafo3() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RingList<Itinerario> itinerarioLlegadaAParada(String idParada) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RingList<Itinerario> itinerarioSalidaAParada(String idParada) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOStop darParadaMasCongestionada() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RingList<VOStop> encontrarCaminoMenorLongitud(String idOrigen, String idDestino, String horaInicio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RingList<VOTrip> encontrarCaminosMenorTiempo(String idOrigen, String idDestino, String horaInicio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CC darCCMasGrande() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RingList<VOStop> darCicloMasLargo(String idParada, String horaInicio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GrafoDirigido darMSTauxiliar(String horaInicio) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	
	
	//------------------------------------
	// METODO MAIN
	//------------------------------------

	public static void main(String[] args) 
	{
		STSManager variable = new STSManager();


		//		long startTime3A = System.currentTimeMillis();
		//		
		//		
		//		 long endTime3A = (System.currentTimeMillis() - startTime3A);
		//		 System.out.println("El tiempo de estimar el punto 3A es: "+endTime3A+" mili segundos");


		variable.cargarGTFS();
		variable.cargarTR("20170821");
		variable.init();



	}



}
