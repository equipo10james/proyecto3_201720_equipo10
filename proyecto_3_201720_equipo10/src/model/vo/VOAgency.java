package model.vo;

public class VOAgency 
{
	private String agency_id;
	private String agency_name;
	private String agency_url;
	private String agency_timezone;
	private String agency_lang;
	
	public VOAgency(String pagency_id, String pagency_name, String pagency_url, String pagency_timezone, String pagency_lang)
	{
		agency_id = pagency_id;
		agency_name = pagency_name;
		agency_url = pagency_url;
		agency_timezone = pagency_timezone;
		agency_lang = pagency_lang;
	}
	
	public String agency_id()
	{
		return agency_id;
	}
	
	public String agency_name()
	{
		return agency_name;
	}

	public String agency_url()
	{
		return agency_url;
	}
	
	public String agency_timezone()
	{
		return agency_timezone;
	}
	
	public String agency_lang()
	{
		return agency_lang;
	}
	
	public String toString()
	{
		return agency_name;
	}
}
