package model.vo;

import model.data_structures.RingList;

public class Retornable 
{
 
	private VOStop parada;
	
	private RingList<VORoute> rutas;
	
	public Retornable(VOStop pParada, RingList<VORoute> pRutas)
	{
		parada = pParada;
		
		rutas = pRutas;
	}
	
	public void agregarRuta(VORoute ruta)
	{
		rutas.addLast(ruta);
	}
	
	public VOStop darParada()
	{
		return parada;
	}
	
	public RingList<VORoute> darRutas()
	{
		return rutas;
	}
	
	public String toString()
	{
		String rep = parada +" :";
		
		for (int i = 0; i < rutas.size(); i++) 
		{
			rep += ((VORoute) rutas.getElement(i)).id() + " ," + ((VORoute) rutas.getElement(i)).darViajes() + " ;" ;
		}
		
		return rep;
	}
}
