package model.vo;

import model.vo.RangoHora;

public class VORetraso 
{
	private VOTrip viaje;
	private VORoute ruta;
	private VOStop parada;
	private RangoHora rangoDeRetraso;
	private String fecha;
	private double segundosRetardados;
	
	public VORetraso (VOStop pParada, VOTrip pViaje, VORoute pRuta, String pFecha, double pSegundos)
	{
		viaje = pViaje;
		ruta = pRuta;
		fecha = pFecha;
		segundosRetardados = pSegundos;
		parada = pParada;
	}
	
	
	public VOTrip viaje()
	{
		return viaje;
	}
	
	public VORoute ruta()
	{
		return ruta;
	}
	
	public RangoHora darRango()
	{
		return rangoDeRetraso;
	}
	
	public String darFecha()
	{
		return fecha;
	}
	public VOStop parada()
	{
		return parada;
	}
	
	public double darSegundosRetrasados()
	{
		return segundosRetardados;
	}
}
