package model.vo;

public class VOCalendarDates 
{
	private String service_id;
	private String date;
	private String exception_type;
	
	public VOCalendarDates(String pService_id, String pDate, String pException)
	{
		service_id = pService_id;
		date = pDate;
		exception_type = pException;
	}
	
	public String service_id()
	{
		return service_id;
	}
	
	public String date()
	{
		return date;
	}
	
	public String exception_type()
	{
		return exception_type;
	}
	
	public String toString()
	{
		return service_id + "," + date() + "," + exception_type;
	}
}
