package model.vo;

public class Fecha 
{

	private int dia;
	
	private int mes;
	
	private int anio;
	
	private int hora;
	
	private RangoHora franja;
	
	public Fecha(int pdia, int pmes, int panio, int phora)
	{
		dia = pdia;
		mes = pmes;
		anio = panio;
		hora = phora;
	}
	
	public int darDia()
	{
		return dia;
	}
	
	public int darMes()
	{
		return mes;
	}
	
	public int darAnio()
	{
		return anio;
	}
	
	public int darHora()
	{
		return hora;
	}
	
	public RangoHora darRango()
	{
		return franja;
	}
	
}
