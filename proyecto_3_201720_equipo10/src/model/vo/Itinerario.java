package model.vo;

import java.util.Iterator;

import model.data_structures.HashTableChaining;
import model.data_structures.RingList;

public class Itinerario 
{

	private RingList<VORoute> rutas;

	private HashTableChaining<VOTrip, Integer> viajes;

	public Itinerario ()
	{

	}


	public RingList<VORoute> darRutas()
	{
		return rutas;
	}

	public RingList<VOTrip> darViajes()
	{
		RingList<VOTrip> resp = new RingList<VOTrip>();
		
		Iterator<VOTrip> iterador = viajes.keys();
		
		while(iterador.hasNext())
		{
			resp.addLast(iterador.next());
		}
		
		return resp;
	}

}
