package model.vo;

import model.data_structures.RingList;

public class RangoHora implements Comparable
{
	private int horaInicio;
	
	private int horaFinal;
	
	private int duracion;
	
	private RingList<VORetraso> retrasos;
	
	private int numParadasRetraso;//nuevo
	
	private String fecha;
	
	public RangoHora(String pinicial, String pfinal)
	{
		horaInicio = horaAsegundos(pfinal);
		
		horaFinal = horaAsegundos(pinicial);
		
		duracion = horaInicio - horaFinal;
		
		this.DefinirNumParadasRetraso();
	}
	
	public void agregarRetraso(VORetraso retraso)
	{
		retrasos.addLast(retraso);
	}
	
	public void agregarRetrasos(RingList retrasosd)
	{
		retrasos = retrasosd;
	}
	
	public RingList<VORetraso> darRetrasos()
	{
		return retrasos;
	}
	
	public int darInicio()
	{
		return horaInicio;
	}
	
	public int darFinal()
	{
		return horaFinal;
	}
	
	public int darDuracion()
	{
		return duracion;
	}
	

	public boolean estaEnRango(String pphora)
	{
		boolean resp = false;
		int phora = horaAsegundos(pphora);
		if(horaInicio <= phora && phora <= horaFinal)
		{
			resp = true;
		}
			
		return resp;
	}
	public int horaAsegundos(String hora)
	{
		int rta = 0;

		String[] arreglo= hora.split(":");

		String[] segundosAMPM = arreglo[2].split(" ");

		rta = ((3600* Integer.parseInt(arreglo[0]))+(60*Integer.parseInt(arreglo[1]))+(Integer.parseInt(segundosAMPM[0])));

		if(segundosAMPM[1].equalsIgnoreCase("pm"))
		{
			rta += (12*3600);
		}

		return rta;
	}


	public void DefinirNumParadasRetraso()
	{
		RingList<VOStop> paradas = new RingList<VOStop>();
		
		for (int i = 0; i < retrasos.size(); i++) 
		{														// Toca incluir este metodo al inicializar, despues de definir los retrasos
			VOStop cosa = ((VORetraso) retrasos.getElement(i)).parada();
			paradas.addFirst(cosa);
		}
		for (int i = 0; i < paradas.size(); i++) 
		{
			if(estaRepetido(((VOStop) paradas.getElement(i)).id()))
			{
				paradas.removePos(i);
			}
		}
		numParadasRetraso = paradas.size();
	}
	public boolean estaRepetido(String idParada)
	{
		boolean repetido = false;
		int veces = 0;
		for (int i = 0; i < retrasos.size(); i++) 
		{
			if(((VORetraso) retrasos.getElement(i)).parada().id().equals(idParada))
			{
				veces ++;
			}
		}
		if(veces > 1 )
		{
			repetido = true;
		}
		
		return repetido;
	}
	
	public int darNumParadasConRetraso()
	{
		return numParadasRetraso;
	}
	@Override
	public int compareTo(Object rango) {
		// TODO Auto-generated method stub
		int rta = 0;
		if(numParadasRetraso > ((RangoHora) rango).darNumParadasConRetraso())
		{
			rta = 1;
		}
		else if(numParadasRetraso < ((RangoHora) rango).darNumParadasConRetraso())
		{
			rta = -1;
		}
		return rta;
	}
	public String darFecha()
	{
		return fecha;
	}
}
