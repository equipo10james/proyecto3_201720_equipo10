package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class RingList <E> implements Iterable<E>
{
	public static class Node<E>
	{
		private E elemento;
		private Node<E> prev;
		private Node<E> next;

		public Node(E e, Node<E> p, Node<E> n)
		{
			elemento = e;
			prev = p;
			next = n;
		}

		public E getElement()
		{
			return elemento;
		}

		public Node<E> getPrev() 
		{
			return prev;
		}

		public Node<E> getNext()
		{
			return next;
		}

		public void setPrev(Node<E> p )
		{
			prev = p;
		}

		public void setNext(Node<E> n)
		{
			next = n;
		}

	}


	private Node<E> head;
	private int size=0;
	private Node<E> current;

	public RingList()
	{
		head = null;
		size = 0;
	}
	public int size()
	{
		return size;
	}
	public boolean isEmpty()
	{
		return size == 0;
	}

	public E first()
	{
		if(isEmpty())
		{
			return null;
		}
		return head.getElement();

	}



	public void addFirst(E cosa)
	{
		Node nodo = new Node<E>(cosa, null, null);
		if(head == null)
		{
			head=nodo;
			head.setNext(head);
			head.setPrev(head);

		}
		else
		{
			nodo.setNext(head);
			nodo.setPrev(head.getPrev());
			head.setPrev(nodo);
			nodo.getPrev().setNext(nodo);
			head = nodo;
		}
		size ++;
	}

	public void addLast(E cosa)
	{
		Node nodo = new Node<E>(cosa, null, null);
		if(head == null)
		{
			head=nodo;
			head.setNext(head);
			head.setPrev(head);

		}
		else
		{
			nodo.setNext(head);
			nodo.setPrev(head.getPrev());
			head.getPrev().setNext(nodo);
			head.setPrev(nodo);

		}
		size ++;
	}

	public void removeFirst()
	{

		if(head != null)
		{
			if(head.getNext() == head)
			{
				head = null;
			}
			else
			{
				head.getNext().setPrev(head.getPrev());
				head.getPrev().setNext(head.getNext());
				head = head.getNext();
			}
			size --;
		}


	}
	public void removeLast()
	{

		if(head != null)
		{
			if(head.getNext() == head)
			{
				removeFirst();
			}
			else
			{
				head.getPrev().getPrev().setNext(head);
				head.setPrev(head.getPrev().getPrev());
				size --;
			}

		}


	}
	public Node getNode(int i)
	{
		if(i > size)
		{
			i = i % size;
		}
		Node p = head;
		int contador = 1;
		while(contador < i)
		{
			p = p.getNext();
			contador ++;
		}

		return p;

	}
	
	public boolean contains(E cosa)
	{
		boolean rta = false;
		Node p = head;
		if (cosa != null)
		{
			 while(p.elemento != cosa && p.next != null)
			 {
				 p = head.next;
				 if (p.elemento.equals(cosa)) 
				 {
					rta = true;
				}
			 }
		}
	
		
		return rta;
	}

	public void addPos(E cosa, int pos)
	{
		if(pos == 1)
		{
			addFirst(cosa);
		}
		else
		{
			Node nuevo = new Node<E>(cosa, null, null);
			Node nodoPos = getNode(pos);
			nuevo.setNext(nodoPos);
			nuevo.setPrev(nodoPos.getPrev());
			nodoPos.getPrev().setNext(nuevo);
			nodoPos.setPrev(nuevo);

		}
		size ++;

	}

	public void removePos( int pos)
	{
		if (pos == size)
		{
			removeLast();
		}
		else if(pos == 1)
		{
			removeFirst();
		}
		else
		{
			Node nodoPos = getNode(pos);
			nodoPos.getPrev().setNext(nodoPos.getNext());
			nodoPos.getNext().setPrev(nodoPos.getPrev());
			size --;	
		}

	}
	public void previous() 
	{
		current = current.getNext();
	}


	public void next() 
	{
		current = current.getPrev();
	}


	public Object getCurrentElement() 
	{
		return current.getElement();
	}


	public Object getElement(int pos)
	{
		return getNode(pos).getElement();
	}

	public void delete() 
	{
		//el actual
		current.getPrev().setNext(current.getNext());
		current.getNext().setPrev(current.getPrev());
	}

	public E buscar(E elemento) {
		E rta = null;

		Iterator<E> iter = this.iterator();
		while (iter.hasNext() && rta == null) 
		{
			E currentElement = iter.next();

			if( elemento.equals(currentElement))
			{
				rta = currentElement;
			}
		}

		return rta;
	}
	
	

	public void switchWithNext(int pos)
	{
		Node a = getNode(pos-1);
		Node b = getNode(pos+2);
		Node nodo = getNode(pos);
		Node nodo2 = getNode(pos+1);
		a.setNext(nodo2);
		nodo2.setNext(nodo);
		nodo.setNext(b);
		b.setPrev(nodo);
		nodo.setPrev(nodo2);
		nodo2.setPrev(a);

	}

	public void sort(int exp) 
	{
		if (head != null) 
		{
			head = divide(head, current ,exp);
			Node<E> correccion = head;

			for (int i = 0; i < this.size-1; i++) 
			{
				correccion = correccion.next;
			}
			current = correccion;
		}
	}


	private Node getMidPoint(Node node) 
	{
		assert node != null;

		Node fast = node.next;
		Node slow = node;

		while (fast != null && fast.next != null) 
		{
			slow = slow.next;
			fast = fast.next.next;
		}
		return slow;
	} 


	private Node divide(Node node1, Node node2, int exp)
	{
		if (node1==node2) return node1;

		Node midPoint = getMidPoint(node1);
		Node midpointNext = midPoint.next;
		midPoint.next = null;

		Node n1 = divide(node1, midPoint, exp);
		Node n2 = divide(midpointNext, node2, exp);

		return mergeList(n1, n2, exp);
	}

	private Node mergeList(Node node1, Node node2, int exp) {
		assert node1 != null;
		assert node2 != null;

		Node current = null;
		Node currentHead = null;

		while (node1 != null && node2 != null) 
		{
			if (( (ComparableExp)node1.getElement()).compareTo(node2.getElement(), exp) < 0) 
			{
				if (current == null) 
				{
					currentHead = node1;
					current = node1;
				} 
				
				else 
				{
					current.next = node1;
					current = current.next;
				}
				
				node1 = node1.next;
			} 
			
			else 
			{
				if (current == null) 
				{
					currentHead = node2;
					current = node2;
				} 
				else 
				{
					current.next = node2;
					current = current.next;
				}
				node2 = node2.next;
			}
		}

		current.next = node1 != null ? node1 : node2;

		return currentHead;
	}

	public String toString() 
	{
		String resp = "";
		for (int i = 0; i < this.size(); i++) 
		{
			resp += this.getElement(i).toString() + "\n";
			
		}
		
		return resp;
	}
	

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator<E>();
	}



	private class ListIterator<E> implements Iterator<E>
	{
		private Node<E> currentNode;
		public ListIterator() 
		{
			super();
			this.currentNode = (Node<E>) head;
		}
		@Override
		public boolean hasNext()
		{
			if (currentNode != null && currentNode.getNext() != null)
				return true;
			else
				return false;
		}
		@Override
		public E next()
		{
			if (!hasNext())
				throw new NoSuchElementException();
			E node = currentNode.getElement();
			currentNode = currentNode.getNext();
			return node;
		}

		@Override
		public void remove() 
		{
			// TODO Auto-generated method stub
			currentNode = null;
		}
	}

}
