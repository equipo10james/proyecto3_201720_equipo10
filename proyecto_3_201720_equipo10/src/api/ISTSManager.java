package api;

import Grafos.CC;
import Grafos.GrafoDirigido;
import model.data_structures.RingList;
import model.data_structures.RojoNegro;
import model.vo.Itinerario;
import model.vo.RangoHora;
import model.vo.Retornable;
import model.vo.VORetraso;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOTransbordo;
import model.vo.VOTrip;

public interface ISTSManager 
{
	/**
	 * Carga toda la informacion estatica necesaria para la operacion del sistema.
	 * (Archivo de rutas, viajes, paradas, etc.)
	 */
	public void cargarGTFS( );

	/**
	 * Carga la informacion en tiempo real de los buses para fecha determinada.
	 * @param fecha
	 */
	public void cargarTR(String fecha); 

	/**
	 * Inicializa estrucuturas de datos y carga toda la información necesaria
	 */
	public void init();
	
	/**
	 * Retorne una lista de todas las paradas alcanzables desde dicha parada.
	 * @param idParada
	 * @param fecha
	 * @return RingList de Itinerario
	 */
	public RingList<VOStop> darParadasAlcanzables(String idParada, String fecha);

	/**
	 * Dar una lista de los componentes fuertemente conexos para toda la red de buses 
	 * (utilizando la información estática de las rutas)
	 * @return RingList de CC.
	 */
	public RingList<CC> darComponentesFuertementeConexos();

	/**
	 * Consultar las paradas que definen el subgrafo. Por cada parada mostrar su identificador y 
	 * las rutas que tienen viajes a la parada.	
	 * @return RingList de VOStop.
	 */
	public RingList<VOStop> darParadasGrafo3( );

	/**
	 * Consultar el itinerario de llegada de buses a una parada en el subgrafo.
	 * @param idParada
	 * @return RingList de Itinerario
	 */
	public RingList<Itinerario> itinerarioLlegadaAParada(String idParada); 

	/**
	 * Consultar el itinerario de salida de buses desde una parada en el subgrafo.
	 * @param idParada
	 * @return RingList de Itinerario
	 */
	public RingList<Itinerario> itinerarioSalidaAParada(String idParada) ;

	/**
	 * Consultar la información de la parada más congestionada 
	 * (aquella tal que tenga la mayor cantidad de buses llegando y/o saliendo). 
	 * @return VOStop
	 */
	public VOStop darParadaMasCongestionada ();

	/**
	 * Encontrar el camino de menor longitud (menor número de arcos) para ir de la parada origen a una 
	 * parada destino saliendo en algún momento a partir de una hora inicial 
	 * (HH:MM definido en la franja horaria).
	 * @param idOrigen
	 * @param idDestino
	 * @param horaInicio
	 * @return RingList de VOStop
	 */
	public RingList<VOStop> encontrarCaminoMenorLongitud(String idOrigen, String idDestino, String horaInicio); 



	/**
	 * Encontrar el camino de menor tiempo para ir de la parada origen a una parada destino saliendo en algún momento 
	 * a partir de una hora inicial (HH:MM en la franja horaria)
	 * @param idOrigen
	 * @param idDestino
	 * @param horaInicio
	 * @return RingList de VOStop
	 */
	public RingList<VOTrip> encontrarCaminosMenorTiempo (String idOrigen, String idDestino, String horaInicio);

	/**
	 * Retorna un arbol con los retardos de un viaje con id idViaje dado por parametro 
	 * en la fecha dada.
	 * @param idViaje
	 * @return La componente fuertemente conexa más grande
	 */
	public CC darCCMasGrande ();

	/**
	 * Retorna una lista con todas las paradas del sistema que son compartidas 
	 * por mas de una ruta en una fecha determinada.
	 * @param fecha
	 * @return RingList de  VOStop 
	 */
	public RingList<VOStop> darCicloMasLargo (String idParada,  String horaInicio);

	/**
	 * Encontrar el Arbol de Recubrimiento Mínimo (MST)
	 * @param horaInicio
	 * @return RingList de MST
	 */
	public GrafoDirigido darMSTauxiliar (String horaInicio);



}
