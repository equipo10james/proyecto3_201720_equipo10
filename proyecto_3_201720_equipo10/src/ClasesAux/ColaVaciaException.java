package ClasesAux;

public class ColaVaciaException extends Exception
{
// -----------------------------------------------------------------
// Constantes
// -----------------------------------------------------------------

/**
* Constante para la serialización
*/
private static final long serialVersionUID = 1L;

// -----------------------------------------------------------------
// Constructores
// -----------------------------------------------------------------
/**
* Constructor con mensaje
* @param mensaje Mensaje de error
*/
public ColaVaciaException( String mensaje )
{
super( mensaje );
}
}
 
