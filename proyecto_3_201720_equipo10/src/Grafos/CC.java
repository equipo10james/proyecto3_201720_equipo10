package Grafos;

import java.util.Iterator;

import ClasesAux.IArco;
import ClasesAux.IVertice;
import model.data_structures.HashTableChaining;
import model.data_structures.RingList;
import model.vo.VOStop;

public class CC<K, V extends IVertice<K>, A extends IArco>
{

	private HashTableChaining<K, Vertice<K,V,A>> subVs;
	private Vertice<K,V,A> comienzo; 
	private K key;
	private int size;

	private RingList<VOStop> paradas;
	
	public CC( GrafoDirigido<K,V,A> grafo, Vertice<K,V,A> raiz)
	{
		size = 0;
		comienzo = raiz;
		key = comienzo.darId();
		subVs = new HashTableChaining<K, Vertice<K,V,A>>();
		subVs.put(comienzo.darId(), comienzo);
		
		paradas = new RingList<VOStop>();
	}

	public K getKey()
	{
		return key;
	}

	public Vertice<K,V,A> getStartVertice()
	{
		return comienzo;
	}

	public HashTableChaining<K, Vertice<K,V,A>> getComponent()
	{
		return subVs;
	}

	public void build()
	{
		Iterator<Vertice<K,V,A>> iterator = comienzo.getAdjacents().values();
		while(iterator.hasNext())
		{
			Vertice<K,V,A> ver = iterator.next();
			
			if(subVs.get(ver.getKey()) == null)
			{
				subVs.put(ver.getKey(), ver);
				build(ver);
			}
		}
		
		size = subVs.size();
	}

	private void build(Vertice<K,V,A> vertice)
	{
		Iterator<Vertice<K,V,A>> iterator = vertice.getAdjacents().values();
		
		while(iterator.hasNext())
		{
			Vertice<K,V,A> ver = iterator.next();
			
			if(subVs.get(ver.getKey()) == null)
			{
				subVs.put(ver.getKey(), ver);
				build(ver);
			}
		}
	}
	
	public void agregarParada(VOStop parada)
	{
		paradas.addLast(parada);
	}
	
	
	public RingList<VOStop> darParadas()
	{
		return paradas;
	}

	public int size()
	{
		return size;
	}


}