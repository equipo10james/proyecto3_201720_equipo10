package Grafos;

import ClasesAux.IArco;

public class ArcoCT implements IArco
{
// -----------------------------------------------------------------
// Constantes
// -----------------------------------------------------------------

/**
* Constante para la serialización
*/
private static final long serialVersionUID = 1L;

// -----------------------------------------------------------------
// Atributos
// -----------------------------------------------------------------

/**
* Peso del arco.
*/
private int peso;

// -----------------------------------------------------------------
// Constructores
// -----------------------------------------------------------------


/**
* Constructor por parámetros del arco.
* @param peso Peso del arco.
*/
public ArcoCT( int peso )
{
this.peso = peso;
}

// -----------------------------------------------------------------
// Métodos
// -----------------------------------------------------------------

/**
* Retorna el peso del arco.
* @return El peso del arco.
*/
public int darPeso( )
{
return peso;
}

}
